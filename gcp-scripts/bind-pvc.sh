#!/bin/bash
#
# Copyright © 2018 Daniel Stone
# 
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# 
# Author: Daniel Stone <daniel@fooishbar.org>
#
#
# This script scrapes the GitLab PVCs from an existing cluster, and creates a
# new GCE VM with the PVCs available as bound disks.
#
# This is used whenever destroying and recreating PVCs is required, e.g. when
# recreating the K8s cluster, or changing the disk configuration.

set -e
set -u
set -o pipefail

# Google Cloud Platform project to work in
GCP_PROJECT="fdo-gitlab"

# Geographical zone to create GCE VM in
GCP_REGION="us-east1"
GCP_ZONE="us-east1-b"

# Machine type to create the temporary GCE VM on
GCE_VM_TYPE="n1-standard-8"

# Cluster name, which will be used in e.g. kubectl
GKE_CLUSTER_NAME="fdo-gitlab-prod"


if ! gcloud version >/dev/null 2>/dev/null; then
	echo "gcloud not installed. Please install it from:"
	echo "    https://cloud.google.com/sdk/install"
	exit 1
fi

if ! jq --help >/dev/null 2>/dev/null; then
	echo "jq not installed; please install it first."
	exit 1
fi

if ! helm home >/dev/null 2>/dev/null; then
	echo "Helm not installed; please install it from:"
	echo "    https://github.com/kubernetes/helm"
	exit 1
fi


echo "Checking access to GCP project ${GCP_PROJECT} ..."
if [ -z "$(gcloud projects list --format='value(projectId)' | egrep \^${GCP_PROJECT}\$)" ]; then
	echo "No access to ${GCP_PROJECT} project in GCP"
	echo ""
	echo "Currently authenticated GCP accounts:"
	gcloud auth list
	echo ""
	echo ""
	echo "Please log in to a sufficiently-credentialed GCP account using:"
	echo "    $ gcloud auth login"
	exit 1
fi
GCP_PROJECT_ID="$(gcloud projects describe ${GCP_PROJECT} --format='value(projectNumber)')"

# XXX: Is this actually part of the gcloud get-credentials ABI, or will it change ... ?
#      I couldn't find a way to make it print the kubectl context name.
KUBE_CTX="gke_${GCP_PROJECT}_${GCP_ZONE}_${GKE_CLUSTER_NAME}"

if ! kubectl --context="${KUBE_CTX}" cluster-info >/dev/null 2>&1; then
	echo "Kubernetes cluster '${GKE_CLUSTER_NAME}' does not exist or you do not have authentication to it."
	echo "Please make sure it exists and authenticate with:"
	echo "    $ gcloud container clusters get-credentials"
	exit 1
fi

# First find out how our Kubernetes persistent volumes are stored.
# We work backwards from the named PersistentVolumeClaim to find the backing
# PersistentVolume, and then back from the PV to find the GCE disk it lives
# on.
#
# We don't back up the 'registry-storage' PVC, because that's ephemeral and
# totally misnamed: the registry data is stored in Google Cloud Storage, rather
# than on local disk, as it's too big to sensibly store locally.
#
# However, we still have a 'registry' volume, beacuse it existed and we need
# a locally-attached scratch disk to do backups. The GitLab backup Rake task
# first copies all the data into a local volume, then tars it up, and only then
# uploads it to our backup storage bucket in GCS. We use registry for this.
gce_disks=""
for i in config-storage postgresql-storage redis-storage storage; do
	pvc_name="gitlab-prod-gitlab-${i}"
	pv_name="$(kubectl --context="${KUBE_CTX}" get -o json pvc/"${pvc_name}" | jq -r '.spec.volumeName')"
	gce_disk_name="$(kubectl --context="${KUBE_CTX}" get -o json pv/"${pv_name}" | jq -r '.spec.gcePersistentDisk.pdName')"
	gce_disks="${gce_disks}${i} ${gce_disk_name}"

done

SNAPSHOT_DATE="$(date --utc '+%Y%m%d%H%M%S')"
SNAPSHOTS=""
echo "Tagging snapshots as fdo-gitlab-backup-\$i-${SNAPSHOT_DATE}"

# We then take a snapshot of the underlying GCE disk.
OLD_IFS=$IFS
export IFS=""
for i in ${gce_disks}; do
	pvc_name="$(echo ${i} | awk '{ print $1; }')"
	gce_name="$(echo ${i} | awk '{ print $2; }')"
	snap_name="fdo-gitlab-backup-${pvc_name}-${SNAPSHOT_DATE}"
	gcloud --project="${GCP_PROJECT}" compute disks snapshot "${gce_name}" \
		--async --zone="${GCP_ZONE}" --snapshot-names="${snap_name}"
	SNAPSHOTS="${SNAPSHOTS}${snap_name} "
done

# Once the snapshot has been created, we use that snapshot to create a new
# ephemeral GCE disk that we can attach to a VM.
GCE_VM_DISKS=""
export IFS=" "
for snap_name in $SNAPSHOTS; do
	snap_status="not ready"
	while ! [ "${snap_status}" = "READY" ]; do
		snap_status="$(gcloud --project="${GCP_PROJECT}" compute snapshots describe "${snap_name}" --format='value(status)')"
		echo "GCE snap ${snap_name} is ${snap_status}"
		sleep 5
	done
	echo "GCE snap ${snap_name} is now ready"
	gcloud --project="${GCP_PROJECT}" compute snapshots add-labels \
		"${snap_name}" --labels="fdo_gitlab_snap_date=${SNAPSHOT_DATE}"

	# Give ourselves 1GiB headroom
	snap_size="$(gcloud --project="${GCP_PROJECT}" compute snapshots describe "${snap_name}" --format='value(storageBytes)')"
	snap_size="$(( snap_size + 1073741824 ))"

	gcloud --project="${GCP_PROJECT}" compute disks create "${snap_name}" \
		--zone="${GCP_ZONE}" --source-snapshot="${snap_name}" \
		--type=pd-ssd --labels="fdo_gitlab_snap_date=${SNAPSHOT_DATE}"
	
	GCE_VM_DISKS="${GCE_VM_DISKS}--disk=name=${snap_name},device-name=${snap_name},auto-delete=yes "
done

export IFS=$OLD_IFS

GCE_VM_NAME="fdo-gitlab-backup-temp-${SNAPSHOT_DATE}"

gcloud --project="${GCP_PROJECT}" compute instances create \
	"${GCE_VM_NAME}" --labels="fdo_gitlab_snap_date=${SNAPSHOT_DATE}" \
	--description="Temporary VM to migrate disks from ${SNAPSHOT_DATE}" \
	--tags=ephemeral --machine-type="${GCE_VM_TYPE}" --zone="${GCP_ZONE}" \
	${GCE_VM_DISKS}

vm_status="not ready"
while ! [ "${vm_status}" = "RUNNING" ]; do
	vm_status="$(gcloud --project="${GCP_PROJECT}" compute instances describe --zone="${GCP_ZONE}" "${GCE_VM_NAME}" --format='value(status)')"
	echo "VM status is ${vm_status}"
	sleep 5
done

echo "VM is running at $(gcloud --project="${GCP_PROJECT}" compute instances describe --zone="${GCP_ZONE}" "${GCE_VM_NAME}" --format='value(networkInterfaces.accessConfigs.natIP)')"

# Future:
#  - actually mount the devices rather than relying on that being manual
#  - spin up a GKE Pod with the new PVCs attached to it
#  - even better would be to automate the copy between them
