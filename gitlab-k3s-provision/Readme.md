# Description

This will create a k3s cluster on packet (equinix metal) with the following:
- use of `c2.medium.x86` packet devices
- single control plane
- multiple agents
- inter-agent communication through `wireguard` (flannel-wireguard extension)
- `kilo` as a VPN server to add peers to configure the cluster
- `packet-ccm` + `metallb` to have a LoadBalancer and automatic Elastic IPs
- `csi-packet` to have integrated k8s storage from packet
- `rook+ceph` to provide a CSI with the local disks on the server/agents
- k8s dashboard

# Prerequesites

## Connect the local machine to the cluster:

```bash
./scripts/add_peer.sh $USER-$(hostname) > fdo-k3s.conf

chmod 600 fdo-k3s.conf
wg-quick up ./fdo-k3s.conf

# fetch a k3s config file
mkdir -p ~/.kube/
./scripts/mk_kubeconfig.sh $USER-$(hostname) > ~/.kube/k3s.yaml

KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods
```

# Access the dashboard
```bash
# retrieve the token
KUBECONFIG=~/.kube/k3s.yaml kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token

KUBECONFIG=~/.kube/k3s.yaml kubectl proxy
```

Now go to http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

# Access the rook+ceph dashboard

The address of the dashboard can be retrieve with the following:

```bash
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get svc rook-ceph-mgr-dashboard

# get the password
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get secrets \
    rook-ceph-dashboard-password -o jsonpath='{.data.password}' \
    | base64 -d
```

Then open a browser to the `CLUSTER-IP` shown above.

# Add an agent to the cluster
```bash
export PACKET_PROJECT_ID=@REPLACE_ME@
export PACKET_DEVICE_NAME=fdo-k3s-agent-42

./gitlab-k3s-packet.sh

# wait for the host to be ready

# attach the agent to the cluster
./scripts/register_agent.sh $PACKET_DEVICE_NAME

# watch the pods getting ready
KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods -o wide -w
```

# Re-deploy the k3s server
Note this will create an entire new cluster with one master

```bash
export PACKET_PROJECT_ID=@REPLACE_ME@
export PACKET_PROJECT_TOKEN=@REPLACE_ME@
export PACKET_DEVICE_NAME=fdo-k3s-server-1

./gitlab-k3s-packet.sh --server

SERVER_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$PACKET_DEVICE_NAME'") | .ip_addresses[0].address')

# wait for the host to be ready

# update the packet cloud secret to finish the setup
cat <<EOF | ssh root@$SERVER_IP \
      kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: packet-cloud-config
  namespace: kube-system
stringData:
  cloud-sa.json: |
    {
    "apiKey": "$PACKET_PROJECT_TOKEN",
    "projectID": "$PACKET_PROJECT_ID"
    }
EOF
```
