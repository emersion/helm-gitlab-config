#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>

import click
import os
import os.path
import requests
import yaml

# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
  if len(data.splitlines()) > 1:  # check for multiline string
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
  return dumper.represent_scalar('tag:yaml.org,2002:str', data)

yaml.add_representer(str, str_presenter)


class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

def get_local_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text

cloud_init = {
    'package-update': True,
    'package-upgrade': True,
    'apt': {
        'sources': {
            'buster-backports.list': {
                'source': 'deb http://deb.debian.org/debian buster-backports main contrib non-free',
            },
        },
    },
    'packages': [
        # HTTPS archive verification
        'debian-archive-keyring',
        'apt-transport-https',
        # sensible updates
        'needrestart',
        # ??
        'patch',
        # network
        'wireguard',
        'ufw',
        # ceph
        'lvm2',
        # to install kgctl
        'git',
        'golang',
        # for helmify
        'tree',
        # daniels
        'zsh',
        'vim',
    ],
    'runcmd': [
        # the upgrade by cloud-init doesn't upgrade the kernel
        'env DEBIAN_FRONTEND=noninteractive apt-get update',
        'env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade',

        # kernel-img.conf creates /boot/initrd.img when our grub expects /boot/initrd
        'ln -sf initrd.img /boot/initrd',

        # reboot to the new kernel if we need
        # we clean cloud-init data so that it re-runs next time
        'if [ -e /var/run/reboot-required ] ; then cloud-init clean ; rm /etc/apt/sources.list.d/buster-backports.list ; reboot ; sleep 60 ; fi',

        # configure RAID for k3s storage
        # first disk is used for k3s images
        # other disks are used for k3s data in longhorn
        'mkdir -p /run/cloudinit',
        '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and .size < 480000000000)) | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_SSD''',
        'echo FREE_SSD $(cat /run/cloudinit/FREE_SSD)',
        'for DEV in $(cat /run/cloudinit/FREE_SSD) ; do echo create partition on $DEV; echo type=83 | sfdisk $DEV; done',
        'mkfs.ext4 -F $(cat /run/cloudinit/FREE_SSD)1',
        'echo \'UUID="\'$(blkid -s UUID -o value $(cat /run/cloudinit/FREE_SSD)1)\'" /var/lib/rancher ext4 defaults 0 0\' | tee -a /etc/fstab',
        'mkdir -p /var/lib/rancher',
        'mount -a',

        # switch to iptables-legacy, k3s doesn't support (yet) nftables
        'update-alternatives --set iptables /usr/sbin/iptables-legacy',
        'update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy',

        # configure ufw
        'ufw allow ssh',
        'ufw allow 51820/udp',  # wireguard
        'ufw default deny incoming',
        'ufw default allow outgoing',
        'ufw default allow routed',
        'ufw allow from 10.42.0.0/16 to any',
        'ufw allow from 10.43.0.0/16 to any',
        'ufw allow from 10.0.0.0/8 proto tcp to any port 10250',  # metrics
        # wireguard external (kilo)
        'ufw allow 51821/udp',  # wireguard
        'ufw allow in on kilo0 from 10.0.0.0/8',
        'systemctl enable --now ufw',
        'ufw enable',
    ],
    'write_files': [
        {
            'path': '/etc/kernel-img.conf',
            'owner': 'root:root',
            'content': 'do_symlinks=Yes\nimage_dest=/boot',
        },
    ]
}


def add_k3s_master(instance_name, channel):
    KUSTOMIZE = 'v3.8.7'
    HELM = 'v3.4.1'
    HELMFILE = 'v0.135.0'
    cloud_init['write_files'].extend([
        {
            'path': '/root/k3s-config.yaml',
            'owner': 'root:root',
            'permissions': '0644',
            'content': get_local_file('k3s-config.yaml')
        },
    ])
    cloud_init['runcmd'].extend([
        # k3s (https://rancher.com/docs/k3s/latest/en/quick-start/)
        'ufw allow from 10.0.0.0/8 proto tcp to any port 6443',  # k3s API

        # fetch the IPs
        '''ip -j -4 addr show label bond0 | jq -r '.[].addr_info | map(select(.local != null)) | .[].local' > /run/cloudinit/PUBLIC_IP''',
        '''ip -j -4 addr show label bond0:0 | jq -r '.[].addr_info | map(select(.local != null)) | .[].local' > /run/cloudinit/PRIVATE_IP''',

        # install k3s
        f'curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL={channel} sh -s server --advertise-address $(cat /run/cloudinit/PRIVATE_IP) --bind-address $(cat /run/cloudinit/PRIVATE_IP) --config /root/k3s-config.yaml',
        'sleep 5',  # give a little bit of time for k3s to spin up

        f'''echo "curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL={channel} K3S_TOKEN=$(cat /var/lib/rancher/k3s/server/token) K3S_URL=https://$(cat /run/cloudinit/PRIVATE_IP):6443 sh -s - --kubelet-arg cloud-provider=external --node-ip \$(ip -j -4 addr show label bond0:0 | jq -r '.[].addr_info | map(select(.local != null)) | .[].local')" > /root/agent.sh''',

        # annotate node for kilo
        'kubectl label node $(hostname) kilo=true',
        'kubectl annotate node fdo-k3s-server-1 kilo.squat.ai/leader=true',

        # fetch kustomize
        f'curl -LO https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F{KUSTOMIZE}/kustomize_{KUSTOMIZE}_linux_amd64.tar.gz',
        f'tar xvf kustomize_{KUSTOMIZE}_linux_amd64.tar.gz -C /usr/local/bin kustomize',

        # install helm, helm diff, helmfile
        f'curl -LO https://get.helm.sh/helm-{HELM}-linux-amd64.tar.gz',
        f'tar xvf helm-{HELM}-linux-amd64.tar.gz -C /usr/local/bin --strip-components 1 linux-amd64/helm',

        'HOME=/root helm plugin install https://github.com/databus23/helm-diff --version master',

        f'curl -L -o /usr/local/bin/helmfile https://github.com/roboll/helmfile/releases/download/{HELMFILE}/helmfile_linux_amd64',
        'chmod +x /usr/local/bin/helmfile',

        # install all prerequisites at once
        # note: it won't be ready until the secret with the API token is provided
        'git clone https://gitlab.freedesktop.org/freedesktop/helm-gitlab-config.git /root/helm-gitlab-config',
        'HOME=/root KUBECONFIG=/etc/rancher/k3s/k3s.yaml helmfile -f /root/helm-gitlab-config/gitlab-k3s-provision/deploy/helmfile.yaml sync',
    ])

@click.command()
@click.option('--instance-name',
              type=click.STRING,
              required=True,
              help='Name for this instance')
@click.option('--server', default=False, is_flag=True, help='Install the k3s binary as a server node.')
@click.option('--channel',
              type=click.STRING,
              default='v1.20',
              help='The release channel to pick the k3s binary from')
def main(instance_name, server, channel):
    if server:
        add_k3s_master(instance_name, channel)

    print('#cloud-config')
    # these options make the result more human readable
    print(yaml.dump(cloud_init, Dumper=MyDumper, indent=2, width=9999, default_flow_style=False))
    print("")

if __name__ == '__main__':
    main()
