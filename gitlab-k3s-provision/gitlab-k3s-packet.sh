#!/bin/sh

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
#  - $PACKET_DEVICE_NAME to the desired device name (e.g. fdo-k3s-5)

packet device create --project-id $PACKET_PROJECT_ID --hostname $PACKET_DEVICE_NAME --plan c2.medium.x86 --facility ewr1 --operating-system debian_10 --userdata "$(./generate-cloud-init.py --instance-name $PACKET_DEVICE_NAME $@)"
