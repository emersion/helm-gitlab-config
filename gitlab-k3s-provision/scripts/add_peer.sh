#!/bin/bash

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
# you can set
#  - $SERVER_NAME which will default to fdo-k3s-server-1
#  - $IP for the requested IP

if [ x"$PACKET_PROJECT_ID" == x"" ]
then
	echo "PACKET_PROJECT_ID not set, aborting." 1>&2
	exit 1
fi

PEER_NAME=$1
SERVER_NAME=${SERVER_NAME:-fdo-k3s-server-1}

if [ x"$PEER_NAME" == x"" ]
then
        echo "usage: $0 PEER_NAME" 1>&2
        exit 1
fi

PRIVATE_KEY=$(mktemp)
PUBLIC_KEY=$(mktemp)

if [ x"$SSH_SERVER_SOCKET" == x"" ]
then
  SSH_SERVER_SOCKET=$(mktemp)
  DELETE_SOCKET="true"
fi

# get the IP of the k3s cluster
if [ x"$SERVER_IP" == x"" ]
then
  SERVER_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$SERVER_NAME'") | .ip_addresses[0].address')
fi
SERVER_PRIVATE_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$SERVER_NAME'") | .ip_addresses[2].address')

if [ x"$SERVER_IP" == x"" ]
then
	echo "can't find $SERVER_NAME on packet" 1>&2
	exit 1
fi

persistent_ssh () {
	ssh -o ControlMaster=auto -o ControlPersist=1h -o ControlPath=$SSH_SERVER_SOCKET root@$SERVER_IP $@
}

echo "connecting to $SERVER_NAME" 1>&2
persistent_ssh "echo connected" 1>&2

# create brand new wireguard keys
wg genkey | tee $PRIVATE_KEY | wg pubkey > $PUBLIC_KEY

# Retrieve the public key of the server
SERVER_KEY=$(persistent_ssh wg show kilo0 public-key)

# Find a non-assigned IP in the 10.6.0.0/24 range:

USED_IPS=$(persistent_ssh \
           "kubectl get -A peers.kilo.squat.ai -o json | \
           jq -r '[.items[]] | sort_by(.metadata.name) | .[].spec.allowedIPs[]'")

if [ x"$IP" != x"" ]
then
	TEST_IP=$IP/32
	for USED_IP in $USED_IPS
	do
		if [ x"$USED_IP" == x"$TEST_IP"]
		then
			echo "$IP already in use, aborting" 1>&2
			TEST_IP=""
		fi
	done
else
	while [ x"$TEST_IP" == x"" ]
	do
		TEST_IP="10.6.0.$(( (RANDOM % 200 ) + 10))/32"
		for USED_IP in $USED_IPS
		do
			if [ x"$USED_IP" == x"$TEST_IP" ]
			then
				TEST_IP=""
			fi
		done
	done
fi

# we have a valid IP
if [ x"$TEST_IP" != x"" ]
then
	(cat <<EOF
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: $PEER_NAME
spec:
  allowedIPs:
  - ${TEST_IP}
  publicKey: $(cat $PUBLIC_KEY)
  persistentKeepalive: 10
EOF
) | persistent_ssh "kubectl apply -f -" 1>&2

	echo "[Interface]
Address = ${TEST_IP}
PrivateKey = $(cat $PRIVATE_KEY)
MTU = 1350

[Peer]
AllowedIPs = 10.42.0.0/16, 10.43.0.0/16, $SERVER_PRIVATE_IP/32, 10.4.0.1/32
Endpoint = $SERVER_IP:51821
PersistentKeepalive = 10
PublicKey = $SERVER_KEY"
fi

# cleanup SSH connection
if [ x"$DELETE_SOCKET" == x"true" ]
then
  ssh -o ControlPath=$SSH_SERVER_SOCKET -O exit foo 1>&2
fi

# clean up temp files
rm $PRIVATE_KEY $PUBLIC_KEY
