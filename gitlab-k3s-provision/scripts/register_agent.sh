#!/bin/bash

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
# you can set
#  - $SERVER_NAME which will default to fdo-k3s-server-1
#  - $IP for the requested IP

if [ x"$PACKET_PROJECT_ID" == x"" ]
then
	echo "PACKET_PROJECT_ID not set, aborting." 1>&2
	exit 1
fi

AGENT_NAME=$1
shift
SERVER_NAME=${SERVER_NAME:-fdo-k3s-server-1}

if [ x"AGENT_NAME" == x"" ]
then
        echo "usage: $0 AGENT_NAME" 1>&2
        exit 1
fi

if [ x"$SSH_SERVER_SOCKET" == x"" ]
then
  SSH_SERVER_SOCKET=$(mktemp)
  DELETE_SOCKET="true"
fi

SSH_AGENT_SOCKET=$(mktemp)

# get the IP of the k3s cluster
if [ x"$SERVER_IP" == x"" ]
then
  SERVER_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$SERVER_NAME'") | .ip_addresses[0].address')
fi
AGENT_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$AGENT_NAME'") | .ip_addresses[0].address')

if [ x"$SERVER_IP" == x"" ]
then
	echo "can't find $SERVER_NAME on packet" 1>&2
	exit 1
fi

if [ x"$AGENT_IP" == x"" ]
then
	echo "can't find $AGENT_NAME on packet" 1>&2
	exit 1
fi

persistent_ssh_server () {
	ssh -o ControlMaster=auto -o ControlPersist=1h -o ControlPath=$SSH_SERVER_SOCKET root@$SERVER_IP $@
}

persistent_ssh_agent () {
	ssh -o ControlMaster=auto -o ControlPersist=1h -o ControlPath=$SSH_AGENT_SOCKET root@$AGENT_IP $@
}

echo "connecting to $SERVER_NAME" 1>&2
persistent_ssh_server "echo connected" 1>&2

echo "connecting to $AGENT_NAME" 1>&2
persistent_ssh_agent "echo connected" 1>&2

# generate kubeconfig for the agent unless NoSchedule is in the args (taint)
KUBECONFIG=$(mktemp)
if [[ "$@" != *"NoSchedule"* ]]
then
  SERVER_IP=$SERVER_IP SSH_SERVER_SOCKET=$SSH_SERVER_SOCKET ./mk_kubeconfig.sh $AGENT_NAME > $KUBECONFIG

  # upload the config file to the agent
  persistent_ssh_agent "mkdir -p /etc/rancher/k3s/"
  scp -o ControlMaster=auto -o ControlPersist=1h -o ControlPath=$SSH_AGENT_SOCKET $KUBECONFIG root@$AGENT_IP:/etc/rancher/k3s/k3s.yaml
fi

REGISTER_COMMAND=$(persistent_ssh_server "cat agent.sh")

persistent_ssh_agent "$REGISTER_COMMAND $@"

# cleanup SSH connection
if [ x"$DELETE_SOCKET" == x"true" ]
then
  ssh -o ControlPath=$SSH_SERVER_SOCKET -O exit foo 1>&2
fi
ssh -o ControlPath=$SSH_AGENT_SOCKET -O exit foo 1>&2

# clean up temp files
rm $KUBECONFIG
