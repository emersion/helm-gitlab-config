#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>

import click
import os
import os.path
import requests
import yaml

# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
  if len(data.splitlines()) > 1:  # check for multiline string
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
  return dumper.represent_scalar('tag:yaml.org,2002:str', data)

yaml.add_representer(str, str_presenter)


class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

def get_local_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text

cloud_init = {
    'package-update': True,
    'package-upgrade': True,
    'apt': {
        'sources': {
            'buster-backports.list': {
                'source': 'deb http://deb.debian.org/debian buster-backports main contrib non-free',
            },
        },
    },
    'packages': [
        # HTTPS archive verification
        'debian-archive-keyring',
        'apt-transport-https',
        # sensible updates
        'needrestart',
        # ??
        'patch',
        # network
        'wireguard',
        'ufw',
        # daniels
        'zsh',
        'vim',
    ],
    'runcmd': [
        # the upgrade by cloud-init doesn't upgrade the kernel
        'env DEBIAN_FRONTEND=noninteractive apt-get update',
        'env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade',

        # kernel-img.conf creates /boot/initrd.img when our grub expects /boot/initrd
        'ln -sf initrd.img /boot/initrd',

        # reboot to the new kernel if we need
        # we clean cloud-init data so that it re-runs next time
        'if [ -e /var/run/reboot-required ] ; then cloud-init clean ; rm /etc/apt/sources.list.d/buster-backports.list ; reboot ; sleep 60 ; fi',

        # make sure Docker is stopped before we rewrite its config
        'systemctl stop docker.service',
        # configure RAID for Docker storage
        'mkdir -p /run/cloudinit',
        '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null)) | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_HDD''',
        '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null)) | map_values("/dev/" + .name + "1") | join(" ")' > /run/cloudinit/FREE_HDD_PART''',
        '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null)) | length' > /run/cloudinit/COUNT_HDD''',
        'echo FREE_HDD $(cat /run/cloudinit/FREE_HDD)',
        'echo FREE_HDD_PART $(cat /run/cloudinit/FREE_HDD_PART)',
        'echo COUNT_HDD $(cat /run/cloudinit/COUNT_HDD)',
        'for DEV in $(cat /run/cloudinit/FREE_HDD) ; do echo create partition on $DEV; echo type=fd | sfdisk $DEV; done',
        'mdadm --create --verbose /dev/md0 --level=5 --raid-devices=$(cat /run/cloudinit/COUNT_HDD) $(cat /run/cloudinit/FREE_HDD_PART)',
        'mkfs.ext4 -F /dev/md0',
        'echo \'UUID="\'$(blkid -s UUID -o value /dev/md0)\'" /var/lib/docker ext4 defaults 0 0\' | tee -a /etc/fstab',
        'mkdir -p /var/lib/docker',
        'mount -a',
        'systemctl enable --now docker.service',
        'systemctl enable --now gitlab-runner.service',
    ],
    'write_files': [
        {
            'path': '/etc/kernel-img.conf',
            'owner': 'root:root',
            'content': 'do_symlinks=Yes\nimage_dest=/boot',
        },
        {
            'path': '/etc/docker-gc-exclude',
            'owner': 'root:root',
            'content': get_local_file("docker-gc-exclude"),
        },
    ]
}

def add_gitlab_runner(instance_name, registration_token, concurrent):
    cloud_init['write_files'].append({
        'path': '/etc/systemd/system/gitlab-runner.service.d/kill.conf',
        'owner': 'root:root',
        'content': get_local_file("gitlab-runner-systemd-kill.conf"),
    })
    cloud_init['apt']['sources']['gitlab-runner.list'] = {
        'source': 'deb https://packages.gitlab.com/runner/gitlab-runner/debian buster main',
        'key': get_remote_uri('https://packages.gitlab.com/runner/gitlab-runner/gpgkey'),
    }
    cloud_init['packages'].extend(['gitlab-runner'])
    # container runtime
    cloud_init['packages'].extend(['docker.io', 'runc', 'tini', 'cgroupfs-mount'])
    # needed for helper-image bootstrapping
    cloud_init['packages'].extend(['binutils', 'binutils-x86-64-linux-gnu', 'binutils-common', 'cdebootstrap', 'gettext-base'])
    # support qemu for cross-arch building
    cloud_init['packages'].extend(['binfmt-support', 'qemu-user-static/buster-backports'])
    cloud_init['runcmd'].extend([
        f'gitlab-runner register --name {instance_name} --non-interactive --limit {concurrent} --request-concurrency 1 --executor docker --docker-image alpine:latest --docker-privileged --docker-devices /dev/kvm --docker-volumes "/var/cache/gitlab-runner/cache:/cache" --custom_build_dir-enabled=true --registration-token {registration_token} --env "DOCKER_TLS_CERTDIR=" --env "FDO_CI_CONCURRENT={concurrent}" --tag-list "kvm,packet.net" --url https://gitlab.freedesktop.org',
        f'sed -e "s/^concurrent = 1/concurrent = {concurrent}/;" -i /etc/gitlab-runner/config.toml',
        '''sed -e 's/^  executor/  pre_clone_script = \\"eval \\\\\\"\\$CI_PRE_CLONE_SCRIPT\\\\\\"\\"\\n  executor/;' -i /etc/gitlab-runner/config.toml''',
    ])


def add_docker_dfs():
    cloud_init['write_files'].append([
        {
            'path': '/usr/local/sbin/docker-free-space',
            'owner': 'root:root',
            'permissions': '0755',
            'content': get_local_file('docker-free-space.py'),
        },
        {
            'path': '/etc/systemd/system/docker-free-space.service',
            'owner': 'root:root',
            'content': get_local_file('docker-free-space.service'),
        }
    ])
    cloud_init['runcmd'].append('systemctl daemon-reload')
    cloud_init['runcmd'].append('systemctl enable --now docker-free-space.service')
    cloud_init['packages'].extend(['python3-docker', 'python3-click', 'python3-parse', 'python3-git', 'python3-yaml'])

@click.command()
@click.option('--instance-name',
              type=click.STRING,
              required=True,
              help='Name for this instance')
@click.option('--gitlab-runner-registration-token',
              type=click.STRING,
              required=True,
              help='gitlab-runner registration token')
@click.option('--gitlab-runner-concurrent',
              type=click.INT,
              required=True,
              help='Number of gitlab-runner jobs to run concurrently')
def main(instance_name, gitlab_runner_registration_token, gitlab_runner_concurrent):
    add_gitlab_runner(instance_name, gitlab_runner_registration_token, gitlab_runner_concurrent)
    add_docker_dfs()

    print('#cloud-config')
    # these options make the result more human readable
    print(yaml.dump(cloud_init, Dumper=MyDumper, indent=2, width=9999, default_flow_style=False))
    print("")

if __name__ == '__main__':
    main()
